let sensibility;
let insulinRacio;
let fastActingInsulin = null;
let mealInsulinRacio = null;


$( window ).load(function() {


    updateGeneralData();
    $('#edit-button').click(function() {
        let newInsulinUnits = Number(prompt("Insere novo valor de insulina diária:", ""));

        while(isNaN(newInsulinUnits)) {
            alert("Por favor insere um número válido");
            newInsulinUnits = Number(prompt("Insere novo valor de insulina diária:", ""));
        }

        changeInsulinUnits(newInsulinUnits);

    })

    $('#valor-glicemia').change(function(event) {
        calculateFastActingInsulin(event.target.value);
        calculateTotalInsulin();
    });

    $('#h-carbono').change(function(event) {
        calculateMealInsulinRacio(event.target.value);
        calculateTotalInsulin();
    });

});


function updateGeneralData() {
    
    let units = localStorage.getItem('insulin');

    if(units === null) {
        showGeneralData(34);
        return;
    }

    showGeneralData(JSON.parse(units).insulinUnits);
}


function showGeneralData(insulinUnits) {
    

    sensibility = (1700/insulinUnits).toFixed(2);
    insulinRacio = (500/insulinUnits).toFixed(2);

    $('#f-sensibilidade').html(sensibility);
    $('#racio-insulina').html(insulinRacio);
    $('#dose-insulina').html(insulinUnits);

}
  
  
function changeInsulinUnits(units) {
      
    const insulin = {
      insulinUnits: units
    }

    localStorage.setItem('insulin', JSON.stringify(insulin));
    updateGeneralData();

    if(fastActingInsulin === null || mealInsulinRacio === null) {
          return;
      }

    calculateFastActingInsulin($('#valor-glicemia').val());
    calculateMealInsulinRacio($('#h-carbono').val());
    calculateTotalInsulin();

  }

function calculateTotalInsulin() {
      if(fastActingInsulin === null || mealInsulinRacio === null) {
          return;
      }

      let totalInsulin = fastActingInsulin + mealInsulinRacio;
      $('#insulina-rapida').html(totalInsulin.toFixed(2));
}

function calculateFastActingInsulin(glycemia) {
    fastActingInsulin = (glycemia - 100) / sensibility;
}

function calculateMealInsulinRacio(hc) {
      mealInsulinRacio = hc / insulinRacio;
}